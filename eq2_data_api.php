<?php
/**
 * PHP wrapper for the SOE REST data api
 *
 * Basic API usage: http://data.soe.com/[s:serviceID]/{format}/{verb}/{game}/{collection}/[{identifier}][?{queryString}]
 * - serviceID is optional and is assigned to registered developers (see http://forums.station.sony.com/station/posts/list.m?topic_id=11500034495 for more info)
 * - format can be one of: xml, json, yaml, yml (same as 'yaml'), or jml (json with extra formatting)
 * - verb can be one of:
 *   - get : retrieve objects matching criteria
 *   - count : retrieve number of objects matching criteria
 *   - status : retrieve status information (currently only used for server status)
 * - game only accepts 'eq2' currently
 * - collection can be one of:
 *   - character
 *   - guild
 *   - item
 *   - spell
 *   - achievement
 *   - alternateadvancement
 *   - alternateadvancementtree
 *   - faction
 *   - warders (only relevant for Beastlords)
 *
 * Server Status usage: [s:serviceID]/{format}/status/{game}[/serverName]
 * serverName is optional and all servers will be returned if it is not specified
 *
 * Game Image API usage: http://data.soe.com/img/{game}/{collection}/{itemInCollection}/{category}[/{itemInCategory}]
 *
 * @author T.W. Ridings <travis@twridings.com>
 * @license None (you're free to use this however you want)
 * @link http://forums.station.sony.com/station/posts/list.m?topic_id=11500034496
 * @link http://twridings.com
 *
 * This software is provided as-is, without warranty, express or implied.
 * It shouldn't cause any problems, harass your friends, or scare your cat
 * but, if it does, you have been warned.
 */
if(!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}
class Eq2DataApi {
	/**
	 * Data Cache directory
	 *
	 * @access public
	 * @var string
	 */
	public $enable_cache = true;
	
	/**
	 * Data Cache directory
	 * This should be an absolute path
	 *
	 * @access public
	 * @var string
	 */
	public $cache_dir = '';
	
	/**
	 * Data Cache lifespan in days
	 *
	 * @access public
	 * @var int
	 */
	public $cache_ttl = 30;
	
	/**
	 * API server address
	 *
	 * @access public
	 * @var string
	 */
	public $api_server = 'data.soe.com';
	
	/**
	 * API Service ID
	 *
	 * @access public
	 * @var string
	 */
	public $serviceID = '';
	
	/**
	 * Game to request data for
	 *
	 * @access public
	 * @var string
	 */
	public $game = 'eq2';
	
	/**
	 * Format of the returned data
	 *
	 * @access public
	 * @var string
	 */
	public $data_format = 'json';
	
	/**
	 * Item data retrieved from the API
	 *
	 * @access public
	 * @var string
	 */
	public $item_data = null;
	
	/**
	 * Image data retrieved from the API
	 *
	 * @access public
	 * @var string
	 */
	public $img_data = null;
	
	/**
	 * Server status data retrieved from the API
	 * 
	 * @access public
	 * @var string
	 */
	public $server_data = null;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param array $params Property values to set.  Available parameters are: 'enable_cache', 'cache_dir', 'cache_ttl', 'api_server', 'serviceID', 'game', and 'data_format'
	 * @return void
	 * @throws Exception if PHP version is < 5
	 */
	public function __construct($params = array()) {
		// Abort if this isn't, at least, PHP5 to avoid errors
		if(defined('PHP_VERSION')) {
			$version = PHP_VERSION;
		} elseif(function_exists('phpversion')) {
			$version = phpversion();
		} else {
			$version = '0';
		}
		if(!version_compare($version, '5.0.0', '>=')) {
			throw new Exception('This class requires PHP version 5+');
		}
		
		if(is_array($params) && count($params)) {
			foreach($params as $prop => $val) {
				if(property_exists($this->{$prop})) {
					$this->{$prop} = $val;
				}
			}
		}
	}
	
	/**
	 * Disallow setting of non-existent properties
	 *
	 * @access public
	 * @throws Exception when trying to set a non-existent property
	 */
	public function __set($prop, $val) {
		throw new Exception('You have attempted to set a value for a property that does not exist.');
	}
	
	/**
	 * Disallow getting of non-existent properties
	 *
	 * @access public
	 * @throws Exception when trying to get a non-existant property
	 */
	public function __get($prop) {
		throw new Exception('You have requested a property that does not exist.');
	}
	
	/**
	 * Lookup an item by ID number or name
	 *
	 * @access public
	 * @param int $id The ID number of the item. Optional. If not supplied, name must be specified.
	 * @param string $name The name of the item. Optional. If not supplied ID must be specified.
	 * @return string Result of the API call
	 * @throws Exception if neither an ID or Name is given, if there is an error connecting to the API server, or a caching error
	 */
	public function lookupItem($id = null, $name = null) {
		if((!$id && !$name) || (empty($id) && empty($name))) {
			throw new Exception('You must supply either an item ID number or name to lookup.');
		}
		
		if($id && !empty($id)) {
			$item_id = filter_var($id, FILTER_SANITIZE_ENCODED);
			
			// lookup the item data
			try {
				$this->getItemDataFromApi('get', 'item', $item_id);
			} catch(Exception $e) {
				throw($e);
			}
		} elseif($name && !empty($name)) {
			$item_name = filter_var($name, FILTER_SANITIZE_ENCODED);
			
			try {
				$this->getItemDataFromApi('get', 'item', null, 'displayname_lower=' . strtolower($item_name));
			} catch(Exception $e) {
				throw($e);
			}
		}
		
		// return the data
		return $this->item_data;
	}
	
	/**
	 * Lookup a character by ID number or name and server
	 *
	 * @access public
	 * @param int $id The ID number of the character. Optional. If not supplied, name must be specified.
	 * @param string $name The first name of the character. Optional. If not supplied ID must be specified.
	 * @param string $server The server of the character to search for.  Optional but required if searching by name.
	 * @param array $filters Filters to apply to the request. Optional.
	 * @return string Result of the API call
	 * @throws Exception if neither an ID or Name/Server is given, if there is an error connecting to the API server, or a caching error
	 */
	public function lookupCharacter($id = null, $name = null, $server = null, $filters = null) {
		if((!$id && !$name) || (empty($id) && empty($name))) {
			throw new Exception('You must supply either a character ID number or name and server to lookup.');
		}
		
		if(!$id && $name) {
			if(empty($name) || (!$server || empty($server))) {
				throw new Exception('You must supply the character and server name when searching by character name.');
			}
		}
		
		if($id && !empty($id)) {
			$char_id = filter_var($id, FILTER_SANITIZE_ENCODED);
			
			// lookup the character data
			try {
				$this->getItemDataFromApi('get', 'character', $char_id, null, $filters);
			} catch(Exception $e) {
				throw($e);
			}
		} elseif($name && !empty($name) && $server && !empty($server)) {
			$char_name = filter_var($name, FILTER_SANITIZE_ENCODED);
			$server_name = filter_var($server, FILTER_SANITIZE_ENCODED);
			
			// lookup the character data
			try {
				$this->getItemDataFromApi('get', 'character', null, 'name.first_lower=' . strtolower($char_name) . '&locationdata.world=' . ucwords($server_name), $filters);
			} catch(Exception $e) {
				throw($e);
			}
		}
		
		// return the data
		return $this->item_data;
	}
	
	/**
	 * Retrieve item icon data from the API
	 *
	 * @access public
	 * @param int $id The ID number of the item to retrieve the icon for
	 * @return string Image icon data or null if nothing was found.
	 * @throws Exception if no item ID is given, if there is an error connecting to the API server, or a caching error
	 */
	public function lookupItemIcon($id = null) {
		if(!$id || empty($id)) {
			throw new Exception('You must supply an item ID number for the item icon to lookup.');
		}
		
		if($id && !empty($id)) {
			$icon_id = filter_var($id, FILTER_SANITIZE_ENCODED);
			
			// lookup the img data
			try {
				$this->getImgDataFromApi('icons', $icon_id, 'item');
			} catch(Exception $e) {
				throw($e);
			}
		}
		
		// return the data
		return $this->img_data;
	}
	
	/**
	 * Retrieve item icon data from the API
	 *
	 * @access public
	 * @param int $id The character ID to get the image from
	 * @param string $type The type of character image to get. Default 'paperdoll'  Valid values are:
	 * - paperdoll     A full picture of the character
	 * - headshot      A picture of the character of from the neck up (suitable for icons)
	 * - housethumb    A picture of a in-game house as published (there may be multiple images returned here)
	 * - petpaperdoll  A a full picture of the character's pet
	 * @param string $item_in_collection Which item from the collection to return (currently only applies to 'housethumb' image type)
	 * @return string Image data in the specified format
	 */
	public function lookupCharacterImg($id = null, $type = 'paperdoll', $item_in_collection = null) {
		if(!$id || empty($id)) {
			throw new Exception('You must supply an character ID number to lookup a character image.');
		}
		
		if(!$type || empty($type)) {
			$type = 'paperdoll';
		}
		
		if($id && !empty($id)) {
			$char_id = filter_var($id, FILTER_SANITIZE_ENCODED);
			
			// lookup the img data
			try {
				$this->getImgDataFromApi('character', $char_id, $type, $item_in_collection);
			} catch(Exception $e) {
				throw($e);
			}
		}
		
		// return the data
		return $this->img_data;
	}

	/**
	 * Get the status of a server (or all servers)
	 * Server status requests are not cached
	 * 
	 * @access public
	 * @param string $server The name of the server to get the status for. If not given all servers will be returned. Default NULL.
	 * @return void
	 * @throws Exception on error connecting to the API
	 */
	public function getServerStatus($server = null) {
		if($server && !empty($server)) {
			$server = ucwords($server);
		}
		
		$url = '';
		
		// build appropriate URL
		if(!empty($this->serviceID)) {
			$url .= 's:'.$this->serviceID . '/';
		}
		
		$url .= $this->data_format .'/status/'. $this->game;
		
		if($server && !empty($server)) {
			$url .= '/' . $server;
		}
		
		try {
			$this->server_data = $this->makeRemoteCall($url);
		} catch(Exception $e) {
			throw($e);
		}
		
		return $this->server_data;
	}
	
	/**
	 * Request Item data from the SOE Data Api with the specified criteria
	 * The format of the data returned is specified by the 'data_format' class property
	 * API call results are automatically cached for the number of days specified in the 'cache_ttl' class property
	 *
	 * @access public
	 * @param string $verb The verb to use. Default 'get'.  Valid values:
	 *   - 'get'    retrieve objects matching criteria
	 *   - 'count'  retrieve number of objects matching criteria
	 *   - 'status' retrieve status information (currently only used for server status)
	 * @param string $collection The collection to query. Default 'item'. Valid values:
	 *   - 'character'
	 *   - 'guild'
	 *   - 'item'
	 *   - 'spell'
	 *   - 'achievement'
	 *   - 'alternateadvancement'
	 *   - 'alternateadvancementtree'
	 *   - 'faction'
	 *   - 'warders' (only relevant for Beastlords)
	 * @param int $identifier The unique identifier to query. Default NULL.
	 * @param string $query_string The query string to use. Default NULL.
	 * @param array $filters The filter commands to use. Default NULL. Valid values are:
	 *   - 'start'       Start with the Nth object within the results of the query (start => 10)
	 *   - 'limit'       Limit the results to at most N objects (limit => 20)
	 *   - 'show'        Only include the provided field(s) from the objects returned (show => field OR show => array(field, field))
	 *   - 'count'       Include the total count of the queried results; compare to the "limit" to see if the results have been constrained by a limit.  The value is either 1 or 0 (default is 0, or "don't show"). (count => 1)
	 *   - 'hide'        Show everything EXCEPT for the provided field(s) from the objects returned (hide => field OR hide => array(field, field))
	 *   - 'sort'        Sort the results by the field(s) provided; using 1 for ascending and -1 for descending (sort => field OR sort => array(field => 1))
	 *   - 'has'         Only include objects where the specified field(s) exist, regardless of the value within that field (has => field)
	 *   - 'resolve'     "Resolve" information by merge data from another collection using provided field(s) (resolve => field OR resolve => array(field, field))
	 *   - 'attachments' Include the attachments for the object for the given category(s) within an attachment_files element (attachments => category)
	 *   - 'explain' 	 "Explain" the database logic that is used for the query to analyze performance (explain => 1)
	 * @return void
	 * @throws Exception on CURL error or if there is a problem during caching (if enabled)
	 */
	public function getItemDataFromApi($verb = 'get', $collection = 'item', $identifier = null, $query_string = null, $filters = null) {
		if(!$verb || empty($verb)) {
			$verb = 'get';
		}
		
		if(!$collection || empty($collection)) {
			$collection = 'item';
		}
		
		$url = '';
		
		// build appropriate URL
		if(!empty($this->serviceID)) {
			$url .= 's:'.$this->serviceID . '/';
		}
		
		$url .= $this->data_format . '/' . $verb . '/' . $this->game . '/' . $collection . '/';
		
		if($identifier && !empty($identifier)) {
			$url .= $identifier;
		}
		
		if($query_string && !empty($query_string)) {
			$url .= '?' . $query_string;
		}
		
		// add filter commands
		if($filters && is_array($filters) && count($filters)) {
			$filter_string = '';
			foreach($filters as $fld => $filter) {
				$filter_string .= '&c:' . $fld . '=';
				
				if(is_array($filter)) {
					foreach($filter as $filter_val) {
						if(is_array($filter_val)) {
							$filter_string .= key($filter_val) . ':' . current($filter_val) . ',';
						} else {
							$filter_string .= $filter_val . ',';
						}
					}
					$filter_string = trim($filter_string, ',');
				} else {
					$filter_string .= $filter;
				}
			}
			
			if(!$query_string || empty($query_string)) {
				$url .= '?';
			} else {
				$url .= '&';
			}
			$url .= trim($filter_string, '&');
		}
		
		if($this->enable_cache) {
			$cache_file_name = md5($url);
			try {
				$this->item_data = $this->getCachedData($cache_file_name);
			} catch(Exception $e) {
				throw($e);
			}
		}
		
		// if the request was not cached make a call to the API
		if(empty($this->item_data) || $this->item_data === false) {
			try {
				$this->item_data = $this->makeRemoteCall($url);
			} catch(Exception $e) {
				throw($e);
			}
			
			if($this->enable_cache) {
				try {
					$this->cacheData($cache_file_name);
				} catch(Exception $e) {
					throw($e);
				}
			}
		}
	}
	
	/**
	 * Request image data from the SOE Data Api with the specified criteria
	 * API call results are automatically cached for the number of days specified in the 'cache_ttl' class property
	 *
	 * @access public
	 * @param string $collection The collection in which to search for the image.  Default 'icons'.
	 * @param int $item_in_collection The ID number of the item to retrieve the image for.
	 * @param string $category The category in the collection to retrieve the image from. Default 'item'.
	 * @param string $item_in_category The specific item in the $category to retrieve the image for.  Optional, used only if there is more than one item in the category.
	 * @return void
	 * @throws Exception on error
	 */
	public function getImgDataFromApi($collection = 'icons', $item_in_collection = null, $category = 'item', $item_in_category = null) {
		if(!$item_in_collection || empty($item_in_collection)) {
			throw new Exception('You must specify the ID number of the item in the collection to retrieve the image for.');
		}
		
		if(!$collection || empty($collection)) {
			$collection = 'icons';
		}
		
		if(!$category || empty($category)) {
			$category = 'item';
		}
		
		$url = '';
		
		// build appropriate URL
		if(!empty($this->serviceID)) {
			$url .= 's:'.$this->serviceID . '/';
		}
		
		$url .= 'img/'. $this->game . '/' . $collection . '/' . $item_in_collection . '/' . $category;
		
		if($item_in_category && !empty($item_in_category)) {
			$url .= '/' . $item_in_category;
		}
		
		if($this->enable_cache) {
			$cache_file_name = md5($url);
			try {
				$this->img_data = $this->getCachedImg($cache_file_name);
			} catch(Exception $e) {
				throw($e);
			}
		}
		
		// if image was not cached request it from the API
		if(empty($this->img_data) || $this->img_data === false) {
			try {
				$this->img_data = $this->makeRemoteCall($url);
			} catch(Exception $e) {
				throw($e);
			}
			
			if($this->enable_cache) {
				try {
					$this->cacheImg($cache_file_name);
				} catch(Exception $e) {
					throw($e);
				}
			}
		}
	}
	
	/**
	 * Clear the currently loaded item, image, and server data
	 * This should be called before each new request when making multiple requests using the same instance
	 *
	 * @access public
	 * @return void
	 */
	public function clearData() {
		$this->item_data = null;
		$this->img_data = null;
		$this->server_data = null;
	}

	/**
	 * Make the call to the remote API
	 * 
	 * @access private
	 * @param string $qry_url The URL to request from the API
	 * @return mixed API response data
	 * @throws Exception on error connecting to the API
	 */
	private function makeRemoteCall($qry_url) {
		// remove leading '/' if present in the query url
		if(substr($qry_url, 0, 1) == '/') {
			$qry_url = substr($qry_url, 0, 1); 
		}
		$api_url = 'http://' . $this->api_server . '/' . $qry_url;
		
		$ch = curl_init();
			
		if($ch === false) {
			throw new Exception('Failed to initialize CURL session.');
		}

		// don't include the response header
		curl_setopt($ch, CURLOPT_HEADER, false);
		
		// return the data as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		curl_setopt($ch, CURLOPT_URL, $api_url);

		$data = curl_exec($ch);
		
		if($data === false) {
			throw new Exception(curl_error($ch), curl_errno($ch));
		}
		
		curl_close($ch);
		
		return $data;
	}
	
	/**
	 * Cache the loaded item data to a txt file
	 *
	 * @access private
	 * @param string $cache_file_name The name of the cache file to write to. Default 'data'
	 * @return mixed Number of bytes written to the cache file or False on failure
	 * @throws Exception if the cache file is not writable
	 */
	private function cacheData($cache_file_name = 'data') {
		$cache = (empty($this->cache_dir)) ? __DIR__:$this->cache_dir;
		
		// if the filename starts with '-', prepend 'x_' to prevent issues
		// on file systems where '-' has meaning (e.g. linux)
		if(substr($cache_file_name, 0, 1) == '-') {
			$cache_file_name = 'x_' . $cache_file_name;
		}
		
		$cache_file_name = str_replace(' ', '_', $cache_file_name);
		
		$cache_file_name .=  '_cache.txt';
		
		if(is_writable($cache)) {
			// open the cache file
			$cache_file = fopen($cache . DS . $cache_file_name, 'wb');
			
			// write the cache data
			$written = fwrite($cache_file, $this->item_data);
			
			// close the cache file
			fclose($cache_file);
			
			return $written;
		} else {
			throw new Exception('Cache directory: "' . $cache . '" is not writable.');
		}
	}
	
	
	/**
	 * Get the contents of a data cache (txt) file
	 * Checks the age of the cache file against the $cache_ttl property value
	 *
	 * @access private
	 * @param string $cache_file_name The cache file to read.  Default 'data'.
	 * @return mixed The contents of the cache file requested or False if the file does not exist, is older than the cache lifespan, or the cache age could not be determined.
	 * @throws Exception if the cache file exists but is not readable
	 */
	private function getCachedData($cache_file_name = 'data') {
		$cache = (empty($this->cache_dir)) ? __DIR__:$this->cache_dir;
		
		// if the filename starts with '-' (as is the case with some item IDs)
		// prepend 'x_' to prevent issues on file systems where '-' has meaning (e.g. linux)
		if(substr($cache_file_name, 0, 1) == '-') {
			$cache_file_name = 'x_' . $cache_file_name;
		}
		
		$cache_file_name = str_replace(' ', '_', $cache_file_name);
		
		$cache_file_name .=  '_cache.txt';
		
		$cache_path = $cache . DS . $cache_file_name;
		
		if(file_exists($cache_path)) {
			try {
				$cache_date = filemtime($cache_path);
				if($cache_date === false) {
					return false;
				}
				
				$cache_mod = new DateTime('@'.$cache_date);
				$now = new DateTime();
				
				$cache_age = $cache_mod->diff($now)->format('%d');
				
				if($cache_age >= intval($this->cache_ttl)) {
					return false;
				}
			} catch(Exception $e) {
				return false;
			}
			
			if(is_readable($cache_path)) {
				return file_get_contents($cache_path);
			} else {
				throw new Exception('Unable to read cache file at ' . $cache_path);
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Cache the image data to a file
	 *
	 * @access private
	 * @param string $cache_file_name The name of the cache file to write to. Default 'img'
	 * @param string $type The type of image file to be cached. Default 'png'
	 * @return mixed Number of bytes written to the cache file or False on failure
	 * @throws Exception if the cache file is not writable
	 */
	private function cacheImg($cache_file_name = 'img', $type = 'png') {
		$cache = (empty($this->cache_dir)) ? __DIR__:$this->cache_dir;
		
		// if the filename starts with '-', prepend 'x_' to prevent issues
		// on file systems where '-' has meaning (e.g. linux)
		if(substr($cache_file_name, 0, 1) == '-') {
			$cache_file_name = 'x_' . $cache_file_name;
		}
		
		$cache_file_name = str_replace(' ', '_', $cache_file_name);
		
		$cache_file_name .=  '_cache.' . $type;
		
		if(is_writable($cache)) {
			// open the cache file
			$cache_file = fopen($cache . DS . $cache_file_name, 'wb');
			
			// write the cache data
			$written = fwrite($cache_file, $this->img_data);
			
			// close the cache file
			fclose($cache_file);
			
			return $written;
		} else {
			throw new Exception('Cache directory: "' . $cache . '" is not writable.');
		}
	}
	
	/**
	 * Get the contents of an image cache file
	 * Checks the age of the cache file against the $cache_ttl property value
	 *
	 * @access private
	 * @param string $cache_file_name The cache file to read.  Default 'img'.
	 * @param string $type The type of cached image to retrieve. Default 'png'.
	 * @return mixed The contents of the cache file requested or False if the file does not exist, is older than the cache lifespan, or the cache age could not be determined.
	 * @throws Exception if the cache file exists but is not readable
	 */
	private function getCachedImg($cache_file_name = 'img', $type = 'png') {
		$cache = (empty($this->cache_dir)) ? __DIR__:$this->cache_dir;
		
		// if the filename starts with '-' (as is the case with some item IDs)
		// prepend 'x_' to prevent issues on file systems where '-' has meaning (e.g. linux)
		if(substr($cache_file_name, 0, 1) == '-') {
			$cache_file_name = 'x_' . $cache_file_name;
		}
		
		$cache_file_name = str_replace(' ', '_', $cache_file_name);
		
		$cache_file_name .=  '_cache.' . $type;
		
		$cache_path = $cache . DS . $cache_file_name;
		
		if(file_exists($cache_path)) {
			try {
				$cache_date = filemtime($cache_path);
				if($cache_date === false) {
					return false;
				}
				
				$cache_mod = new DateTime('@'.$cache_date);
				$now = new DateTime();
				
				$cache_age = $cache_mod->diff($now)->format('%d');
				
				if($cache_age >= intval($this->cache_ttl)) {
					return false;
				}
			} catch(Exception $e) {
				return false;
			}
			
			if(is_readable($cache_path)) {
				return file_get_contents($cache_path);
			} else {
				throw new Exception('Unable to read cache file at ' . $cache_path);
			}
		} else {
			return false;
		}
	}
}
?>
